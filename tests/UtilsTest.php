<?php

use Favez\ORM\Utils;
use PHPUnit\Framework\TestCase;

class UtilsTest extends TestCase
{
    
    public function testSQL()
    {
        $result = Utils::sql('SELECT * FROM %s LIMIT 0', 'human');
        
        $this->assertEquals('SELECT * FROM human LIMIT 0', $result);
    }
    
    public function testCamelize()
    {
        $result = Utils::camelize('human_eye');
        
        $this->assertEquals('HumanEye', $result);
    }
    
    public function testUnderscore ()
    {
        $result = Utils::underscore('HumanEye');
        
        $this->assertEquals('human_eye', $result);
    }
    
}