<?php

use PHPUnit\DbUnit\TestCaseTrait;

class AppTest extends DatabaseTestCase
{
    use TestCaseTrait;

    public function testConnection()
    {
        $db = new \Favez\ORM\App();

        $result = $db->setPDO(parent::$pdo);

        $this->assertEquals(true, $result);
        $this->assertEquals(true, $db->getPDO() instanceof PDO);
        $this->assertEquals(true, $db->getDriver() instanceof FluentPDO);
    }

    protected function getDataSet()
    {
        return $this->createArrayDataSet([]);
    }
}