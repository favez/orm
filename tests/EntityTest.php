<?php

use Favez\ORM\Entity\ArrayCollection;
use Favez\ORM\Entity\Entity;
use Favez\ORM\Entity\Repository;
use PHPUnit\DbUnit\TestCaseTrait;

/**
 * Class EntityTest
 *
 * Tests if an entity can be created, inserted, updated, refreshed fetched, and removed from database.
 *
 * created   => The creation of an empty human entity.
 * inserted  => Inserting a non-empty human to database.
 * updated   => Updating a fetched human to database.
 * refreshed => Refresh the data in a fetched human from database.
 * fetched   => Load a human from database into an entity.
 * removed   => Remove an existing human from database.
 */
class EntityTest extends DatabaseTestCase
{
    use TestCaseTrait;

    /**
     * Tests if the $db->getRepository() returns a valid entity repository.
     */
    public function testRepository()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);

        $this->assertTrue($repository instanceof Repository);
    }

    /**
     * Tests if the instance of a class is correctly created using an entity repository.
     *
     * It must be an instanceof Entity and an instanceof Human.
     */
    public function testCreate()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);
        $human      = $repository->create();

        $this->assertTrue($human instanceof Entity);
        $this->assertTrue($human instanceof \Favez\ORM\Tests\Models\Human);
    }

    /**
     * Tests if the created instance can be converted to an array correctly.
     */
    public function testToArray()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);
        $human      = $repository->create();

        $human->name = 'Adam';

        $values = $human->toArray();

        $this->assertTrue(is_array($values));
        $this->assertTrue(array_key_exists('id', $values));
        $this->assertNull($values['id']);
        $this->assertEquals('Adam', $values['name']);
    }

    /**
     * Tests if the created human can be inserted to database without any problems.
     */
    public function testSave()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);
        $human      = $repository->create();

        $human->name = 'Adam';

        $this->assertNull($human->id);

        try {
            $repository->save($human);
        } catch (Exception $ex) {
            //
        }

        $this->assertTrue($human->id > 0);
        $this->assertTrue(is_integer($human->id));
    }

    /**
     * Tests if the human can be created and updated in database.
     *
     * @throws \Exception
     */
    public function testSaveUpdate()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);

        /** @var \Favez\ORM\Tests\Models\Human $human */
        $human = $repository->findOneBy([ 'name' => 'Adam' ]);

        $this->assertTrue($human instanceof \Favez\ORM\Tests\Models\Human);

        $human->name = 'Eva';

        $this->assertEquals('Eva', $human->name);

        $repository->save($human);

        $human = $repository->find($human->id);

        $this->assertEquals('Eva', $human->name);
    }

    /**
     * Tests if the fetched human can be refreshed.
     */
    public function testRefresh()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);

        /** @var \Favez\ORM\Tests\Models\Human $human */
        $human = $repository->findOneBy([ 'name' => 'Adam' ]);

        $this->assertTrue($human instanceof \Favez\ORM\Tests\Models\Human);

        $humanName   = $human->name;
        $human->name = 'Test';

        $repository->refresh($human);

        $this->assertEquals($humanName, $human->name);
    }

    /**
     * Tests if an existing human can be deleted from database.
     */
    public function testRemove()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);

        /** @var \Favez\ORM\Tests\Models\Human $human */
        $human   = $repository->findOneBy([ 'name' => 'Adam' ]);
        $humanID = $human->id;

        $this->assertTrue($human instanceof \Favez\ORM\Tests\Models\Human);

        $repository->remove($human);

        $this->assertEquals(null, $human->id);

        $human = $repository->find($humanID);

        $this->assertNull($human);
    }

    public function testAssociation()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);

        // Create a new human
        /** @var \Favez\ORM\Tests\Models\Human $human */
        $human       = $repository->create();
        $human->name = 'Daniel';
        $human->eyes = [
            [ 'color' => 'green' ],
            [ 'color' => 'brown' ],
        ];

        $this->assertTrue($human->eyes instanceof ArrayCollection);
        $this->assertEquals(2, $human->eyes->count());
        $this->assertTrue($human->eyes->get(0) instanceof \Favez\ORM\Tests\Models\Eye);
        $this->assertTrue($human->eyes->get(1) instanceof \Favez\ORM\Tests\Models\Eye);

        $eyes = $human->getAssociated('eyes');
        $this->assertEquals($eyes, $human->eyes);

        $repository->save($human);

        $this->assertNotEmpty($human->eyes->get(0)->id);
        $this->assertNotEmpty($human->eyes->get(0)->humanID);
        $this->assertEquals($human->id, $human->eyes->get(0)->humanID);

        $human->eyes->get(0)->color = 'blue';
        $repository->save($human);

        $human->name           = 'Steve';
        $human->eyes->get(0)->color = 'green';
        $repository->refresh($human);

        $this->assertNotEquals('Steve', $human->name);
        $this->assertNotEquals('green', $human->eyes->get(0)->color);

        $this->assertEquals('Daniel', $human->name);
        $this->assertEquals('blue', $human->eyes->get(0)->color);

        $repository->remove($human);

        $this->assertNull($human->id);
        $this->assertNull($human->eyes->get(0)->id);
        $this->assertNull($human->eyes->get(0)->humanID);
    }

    protected function getDataSet()
    {
        return $this->createArrayDataSet(
            [
                'human'           => [
                    [
                        'id'   => 1,
                        'name' => 'Adam',
                    ],
                ],
                'human_eye'       => [

                ],
                'school'          => [
                    [
                        'id'   => 1,
                        'name' => 'Guest School',
                    ],
                    [
                        'id'   => 2,
                        'name' => 'Private School',
                    ],
                ],
                'human_school_ro' => [
                    [
                        'id'       => 1,
                        'humanID'  => 1,
                        'schoolID' => 1,
                    ],
                ],
            ]
        );
    }
}