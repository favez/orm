<?php

use Favez\ORM\Entity\ArrayCollection;
use Favez\ORM\Entity\Entity;
use Favez\ORM\Entity\Repository;
use PHPUnit\DbUnit\TestCaseTrait;

/**
 * Class EntityTest
 */
class ManyToManyTest extends DatabaseTestCase
{
    use TestCaseTrait;

    public function testManyToMany()
    {
        $db         = new \Favez\ORM\App(parent::$pdo);
        $repository = $db->getRepository(\Favez\ORM\Tests\Models\Human::class);

        $human = $repository->find(1);

        $this->assertInstanceOf(\Favez\ORM\Tests\Models\Human::class, $human);

        $schools = $human->schools;

        $this->assertInstanceOf(ArrayCollection::class, $schools);

        $school = $schools->get(0);

        $this->assertInstanceOf(Entity::class, $school);
        $this->assertInstanceOf(\Favez\ORM\Tests\Models\School::class, $school);
        $this->assertEquals(1, $school->id);

        $schools->removeAt(0);

        $this->assertEquals(1, $human->schools->count());

        $repository->save($human);
        $repository->refresh($human);

        $this->assertEquals(2, $human->schools->get(0)->id);
        $this->assertEquals(1, $human->schools->count());
        $this->assertNull($human->schools->get(1));

        $repository->remove($human);
    }

    protected function getDataSet()
    {
        return $this->createArrayDataSet(
            [
                'human'           => [
                    [
                        'id'   => 1,
                        'name' => 'Adam',
                    ],
                ],
                'human_eye'       => [

                ],
                'school'          => [
                    [
                        'id'   => 1,
                        'name' => 'Guest School',
                    ],
                    [
                        'id'   => 2,
                        'name' => 'Private School',
                    ],
                ],
                'human_school_ro' => [
                    [
                        'id'       => 1,
                        'humanID'  => 1,
                        'schoolID' => 1,
                    ],
                    [
                        'id'       => 2,
                        'humanID'  => 1,
                        'schoolID' => 2,
                    ],
                ],
            ]
        );
    }
}