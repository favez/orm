<?php

namespace Favez\ORM\Tests\Models;

use Favez\ORM\Entity\Entity;

class School extends Entity
{
    const SOURCE = 'school';

    public $id;

    public $name;
}