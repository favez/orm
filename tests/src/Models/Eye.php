<?php

namespace Favez\ORM\Tests\Models;

use Favez\ORM\Entity\Entity;

class Eye extends Entity
{
    const SOURCE                     = 'human_eye';

    const SHOULD_REMOVE_WITH_PARENT  = true;

    const SHOULD_UPDATE_WITH_PARENT  = true;

    const SHOULD_REFRESH_WITH_PARENT = true;

    public $id;

    public $humanID;

    public $color;

    public function initialize()
    {
        $this->belongsTo(Human::class, 'humanID', 'id')->setName('human');
    }
}

