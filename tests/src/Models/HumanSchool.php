<?php

namespace Favez\ORM\Tests\Models;

use Favez\ORM\Entity\Entity;

class HumanSchool extends Entity
{
    const SOURCE = 'human_school_ro';

    const SHOULD_UPDATE_WITH_PARENT = true;

    const SHOULD_REFRESH_WITH_PARENT = true;

    const SHOULD_REMOVE_WITH_PARENT = true;

    public $id;

    public $humanID;

    public $schoolID;
}
