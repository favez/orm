<?php

namespace Favez\ORM\Tests\Models;

use Favez\ORM\Entity\Entity;

class Human extends Entity
{
    const SOURCE = 'human';

    public $id;

    public $name;

    public function initialize()
    {
        $this->hasMany(Eye::class, 'humanID', 'id', 2)->setName('eyes');

        $this->manyToMany(
            HumanSchool::class,
            'humanID',
            'schoolID',
            School::class,
            'id'
        )->setName('schools');
    }
}

