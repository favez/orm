<?php

namespace Favez\ORM\Entity\Relation;

use Favez\ORM\App;
use Favez\ORM\Entity\Entity;
use Favez\ORM\Entity\Relation;

abstract class Handler
{
    /**
     * @var \Favez\ORM\App
     */
    protected $app;

    /**
     * The base entity.
     *
     * @var \Favez\ORM\Entity\Entity
     */
    protected $entity;

    /**
     * @var \Favez\ORM\Entity\Relation
     */
    protected $relation;

    /**
     * The name of the reference class.
     *
     * @var string|\Favez\ORM\Entity\Entity
     */
    protected $referenceClass;

    /**
     * The foreign-key of the reference class.
     *
     * @var string
     */
    protected $referenceKey;

    /**
     * The local primary key.
     *
     * @var string
     */
    protected $localKey;

    /**
     * Limit for the has-many relation.
     *
     * @var integer
     */
    protected $limit;

    public function __construct(Entity $entity)
    {
        $this->entity = $entity;
    }

    abstract public function fetch();

    abstract public function processResult($result);

    abstract public function save();

    abstract public function remove();

    abstract public function refresh();

    public function setApp(App $app): void
    {
        $this->app = $app;
    }

    public function setRelation(Relation $relation): void
    {
        $this->relation = $relation;
    }

    public function getEntity(): \Favez\ORM\Entity\Entity
    {
        return $this->entity;
    }

    final public function shouldUpdate(): bool
    {
        return $this->referenceClass::SHOULD_UPDATE_WITH_PARENT;
    }

    final public function shouldRemove(): bool
    {
        return $this->referenceClass::SHOULD_REMOVE_WITH_PARENT;
    }

    final public function shouldRefresh(): bool
    {
        return $this->referenceClass::SHOULD_REFRESH_WITH_PARENT;
    }
}