<?php

namespace Favez\ORM\Entity\Relation\Handler;

use Favez\ORM\Entity\Relation\Handler;

class BelongsToRelationHandler extends Handler
{
    /**
     * @param string $referenceClass
     * @param string $localKey
     * @param string $referenceKey
     */
    public function configure($referenceClass, $localKey, $referenceKey): void
    {
        $this->referenceClass = $referenceClass;
        $this->referenceKey   = $referenceKey;
        $this->localKey       = $localKey;
        $this->limit          = 0;
    }

    public function fetch()
    {
        $repository = $this->app->getRepository($this->referenceClass);
        $reference  = $repository->findOneBy(
            [
                $this->referenceKey => $this->entity->get($this->localKey),
            ]
        );

        return $reference;
    }

    public function processResult($result)
    {
        if (is_array($result)) {
            $repository = $this->app->getRepository($this->referenceClass);
            $reference  = $repository->create();
            $reference->set($result);

            $result = $reference;
        }

        return $result;
    }

    public function save()
    {
    }

    public function refresh()
    {
    }

    public function remove()
    {
    }
}