<?php

namespace Favez\ORM\Entity\Relation\Handler;

use Favez\ORM\Entity\ArrayCollection;
use Favez\ORM\Entity\Entity;
use Favez\ORM\Entity\Relation\Handler;

class ManyToManyRelationHandler extends Handler
{
    protected $referenceTargetKey;

    protected $targetClass;

    protected $targetKey;

    /**
     * @var \Favez\ORM\Entity\Repository
     */
    protected $repository;

    /**
     * @param string $referenceClass
     * @param string $referenceKey
     * @param string $referenceTargetKey
     * @param string $targetClass
     * @param string $targetKey
     */
    public function configure($referenceClass, $referenceKey, $referenceTargetKey, $targetClass, $targetKey)
    {
        $this->referenceClass     = $referenceClass;
        $this->referenceKey       = $referenceKey;
        $this->referenceTargetKey = $referenceTargetKey;
        $this->targetClass        = $targetClass;
        $this->targetKey          = $targetKey;
    }

    /**
     * Do load entities using a relation table.
     *
     * Current => Relation => Target
     *
     * @return array|ArrayCollection
     * @throws \Exception
     */
    public function fetch()
    {
        $targetTable = $this->targetClass::SOURCE;
        $refTable    = $this->referenceClass::SOURCE;

        $query = $this->app->getDriver()
            ->from($this->targetClass::SOURCE)
            ->leftJoin("$refTable ON $refTable.{$this->referenceTargetKey} = $targetTable.{$this->targetKey}")
            ->where("$refTable.{$this->referenceKey} = ?", $this->entity->id);

        $rows       = $query->fetchAll();
        $result     = new ArrayCollection();
        $repository = $this->app->getRepository($this->targetClass);

        foreach ($rows as $row) {
            $model = $repository->create();
            $model->set($row);

            $result->add($model);
        }

        return $result;
    }

    /**
     * @param mixed $result
     *
     * @return \Favez\ORM\Entity\ArrayCollection
     * @throws \Exception
     */
    public function processResult($result): ArrayCollection
    {
        return $result;
    }

    /**
     * Saves related entities
     *
     * @throws \Exception
     */
    public function save()
    {
        $entities = $this->relation->getResult();

        if ($entities instanceof ArrayCollection) {
            $localID    = $this->entity->id;
            $repository = $this->app->getRepository($this->referenceClass);

            // Step 1: Compare existing entities with local entities
            $relations = $repository->findBy(
                [
                    $this->referenceKey => $localID,
                ]
            );

            foreach ($relations->getIterator() as $relation) {
                $referenceId = (int)$relation->{$this->referenceTargetKey};
                $match       = $entities->find(
                    static function ($school) use ($referenceId) {
                        return (int)$school->id === $referenceId;
                    }
                );

                if (!$match) {
                    $repository->remove($relation);
                }
            }

            // Step 2: Upsert local entities
            foreach ($entities->getIterator() as $entity) {
                $key      = $this->referenceTargetKey;
                $entityId = (int)$entity->id;

                $match = $relations->find(
                    static function ($relation) use ($key, $entityId) {
                        return (int)$relation->{$key} === $entityId;
                    }
                );

                if (!$match) {
                    $related                              = $repository->create();
                    $related->{$this->referenceKey}       = $localID;
                    $related->{$this->referenceTargetKey} = $entity->id;

                    $repository->save($related);
                }
            }
        }
    }

    /**
     * Refreshes related entities
     *
     * @throws \Exception
     */
    public function refresh(): void
    {
        $this->relation->setResult(
            $this->fetch()
        );
    }

    /**
     * Removes related entities
     *
     * @throws \Exception
     */
    public function remove()
    {
        $localID = $this->entity->id;

        $this->app->getDriver()->delete($this->referenceClass::SOURCE)
            ->where($this->referenceKey, $localID)
            ->execute();
    }
}