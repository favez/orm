<?php

namespace Favez\ORM\Entity\Relation\Handler;

use Exception;
use Favez\ORM\Entity\ArrayCollection;
use Favez\ORM\Entity\Entity;
use Favez\ORM\Entity\Relation\Handler;

class HasManyRelationHandler extends Handler
{
    
    /**
     * @var \Favez\ORM\Entity\Repository
     */
    protected $repository;
    
    /**
     * @param string  $referenceClass
     * @param string  $referenceKey
     * @param string  $localKey
     * @param integer $limit
     */
    public function configure ($referenceClass, $referenceKey, $localKey, $limit)
    {
        $this->referenceClass = $referenceClass;
        $this->referenceKey   = $referenceKey;
        $this->localKey       = $localKey;
        $this->limit          = $limit;
        $this->repository     = $this->app->getRepository($this->referenceClass);
    }
    
    public function fetch () : ArrayCollection
    {
        $references = $this->repository->findBy([
            $this->referenceKey => $this->entity->get($this->localKey)
        ]);
    
        return $references;
    }
    
    /**
     * @param mixed $result
     *
     * @return array|\Favez\ORM\Entity\ArrayCollection
     * @throws \Exception
     */
    public function processResult ($result)
    {
        if (is_array($result))
        {
            $collection = new ArrayCollection();
            
            foreach ($result as $item)
            {
                if ($item instanceof Entity)
                {
                    $item->set($this->referenceKey, $this->entity->id);
                    
                    $collection[] = $item;
                }
                else if(is_array($item))
                {
                    $model = $this->repository->create();
                    $model->set($item);
                    $model->set($this->referenceKey, $this->entity->id);
                    
                    $collection->add($model);
                }
            }
            
            return $collection;
        }

        if($result instanceof ArrayCollection)
        {
            foreach ($result->getIterator() as $item)
            {
                $item->set($this->referenceKey, $this->entity->id);
            }

            return $result;
        }

        throw new Exception('Unexpected result input.');
    }
    
    /**
     * Saves related entities
     *
     * @throws \Exception
     */
    public function save ()
    {
        $result = $this->relation->getResult();
        
        if (!($result instanceof ArrayCollection))
        {
            return;
        }
        
        foreach ($result->getIterator() as $entity)
        {
            $entity->{$this->referenceKey} = $this->entity->{$this->localKey};
            $this->repository->save($entity);
        }
    }
    
    /**
     * Refreshes related entities
     *
     * @throws \Exception
     */
    public function refresh ()
    {
        $result = $this->relation->getResult();
    
        if (!($result instanceof ArrayCollection))
        {
            return;
        }
    
        foreach ($result->getIterator() as $entity)
        {
            $this->repository->refresh($entity);
        }
    }
    
    /**
     * Removes related entities
     *
     * @throws \Exception
     */
    public function remove ()
    {
        $result = $this->relation->getResult();
    
        if (!($result instanceof ArrayCollection))
        {
            $referenceID = $this->entity->id;
            $this->app->getDriver()->delete($this->referenceClass::SOURCE)
                ->where($this->referenceKey, $referenceID)
                ->execute();
        }
        else
        {
            foreach ($result->getIterator() as $entity)
            {
                $entity->{$this->referenceKey} = null;
                $this->repository->remove($entity);
            }
        }
    }
    
}