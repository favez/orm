<?php

namespace Favez\ORM\Entity\Relation\Handler;

use Exception;
use Favez\ORM\Entity\ArrayCollection;
use Favez\ORM\Entity\Entity;
use Favez\ORM\Entity\Relation\Handler;

class HasOneRelationHandler extends Handler
{

    /**
     * @var \Favez\ORM\Entity\Repository
     */
    protected $repository;

    /**
     * @param string  $referenceClass
     * @param string  $referenceKey
     * @param string  $localKey
     * @param integer $limit
     */
    public function configure ($referenceClass, $referenceKey, $localKey)
    {
        $this->referenceClass = $referenceClass;
        $this->referenceKey   = $referenceKey;
        $this->localKey       = $localKey;
        $this->repository     = $this->app->getRepository($this->referenceClass);
    }

    public function fetch ()
    {
        $reference = $this->repository->findOneBy([
            $this->referenceKey => $this->entity->get($this->localKey)
        ]);

        return $reference;
    }

    /**
     * @param mixed $result
     *
     * @return array|\Favez\ORM\Entity\ArrayCollection
     * @throws \Exception
     */
    public function processResult ($result)
    {
        if (is_array($result))
        {
            $repository = $this->app->getRepository($this->referenceClass);
            $reference  = $repository->create();
            $reference->set($result);
            $reference->set($this->referenceKey, $this->entity->id);

            $result = $reference;
        }

        return $result;
    }

    /**
     * Saves related entities
     *
     * @throws \Exception
     */
    public function save ()
    {
        /** @var Entity $result */
        $result = $this->relation->getResult();

        if ($result instanceof Entity)
        {
            $result->{$this->referenceKey} = $this->entity->{$this->localKey};

            $this->repository->save($result);
        }
    }

    /**
     * Refreshes related entities
     *
     * @throws \Exception
     */
    public function refresh ()
    {
        /** @var Entity $result */
        $result = $this->relation->getResult();

        if ($result instanceof Entity)
        {
            $this->repository->refresh($result);
        }
    }

    /**
     * Removes related entities
     *
     * @throws \Exception
     */
    public function remove ()
    {
        /** @var Entity $result */
        $result = $this->relation->getResult();

        if ($result instanceof Entity)
        {
            $result->{$this->referenceKey} = null;

            $this->repository->remove($result);
        }
        else
        {
            $referenceID = $this->entity->id;
            $this->app->getDriver()->delete($this->referenceClass::SOURCE)
                ->where($this->referenceKey, $referenceID)
                ->execute();
        }
    }

}