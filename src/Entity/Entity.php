<?php

namespace Favez\ORM\Entity;

use Exception;
use Favez\ORM\App;
use Favez\ORM\Entity\Relation\Handler\BelongsToRelationHandler;
use Favez\ORM\Entity\Relation\Handler\HasManyRelationHandler;
use Favez\ORM\Entity\Relation\Handler\HasOneRelationHandler;
use Favez\ORM\Entity\Relation\Handler\ManyToManyRelationHandler;
use Favez\ORM\Utils;

/**
 * Class Entity
 *
 * @package Favez\ORM\Entity
 *
 * @property integer $id
 */
abstract class Entity implements \ArrayAccess
{
    /**
     * Whether the entity should be updated if the parent entity is updated.
     *
     * This is only used in a hasOne or hasMany relation.
     *
     * @var bool
     */
    public const SHOULD_UPDATE_WITH_PARENT = false;

    /**
     * Whether the entity should be removed if the parent entity is removed.
     *
     * This is only used in a hasOne or hasMany relation.
     *
     * @var bool
     */
    public const SHOULD_REMOVE_WITH_PARENT = false;

    /**
     * Whether the entity should be refreshed if the parent entity is refreshed.
     *
     * @var bool
     */
    public const SHOULD_REFRESH_WITH_PARENT = false;

    /**
     * The database table name without prefix.
     *
     * @var string
     */
    public const SOURCE = null;

    /**
     * @var App
     */
    private $__app;

    /**
     * Contains all meta information about the entity such as columns, indexes etc.
     * This information will be cached if a cache provider is available.
     *
     * todo: Create a meta entity class which parses the meta data result for easier access.
     *
     * @var Meta
     */
    private $__meta;

    /**
     * An internal list of relations for this entity.
     *
     * @var Relation[]
     */
    private $__relations;

    final public function __construct(App $app, Meta $meta)
    {
        $this->__app       = $app;
        $this->__meta      = $meta;
        $this->__relations = [];

        $this->initialize();
    }

    /**
     * Called in the constructor. Usually used to register associations.
     */
    public function initialize()
    {
    }

    /**
     * Method to set an entities property or association value.
     * Can be accessed by Entity::__set(string $key, mixed $value)
     *
     * @param mixed $key Property array, property name, association entity class, association alias
     * @param mixed $value
     *
     * @throws \Exception
     */
    final public function set($key, $value = null): void
    {
        if (is_array($key) && $value === null) {
            foreach ($key as $_key => $value) {
                $this->set($_key, $value);
            }

            return;
        }

        if (is_string($key)) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;

                return;
            }

            // Looks for a relation
            foreach ($this->__relations as $relation) {
                if ($relation->getName() === $key) {
                    $relation->setResult($value);

                    return;
                }
            }
        }

        throw new Exception('Access to unknown property/relation: ' . $key);
    }

    /**
     * Magic setter which calls Entity::set
     *
     * @param string $key
     * @param mixed  $value
     *
     * @throws \Exception
     */
    final public function __set($key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * Method to get properties / associated models.
     *
     * @param string $key
     *
     * @return mixed
     * @throws \Exception
     */
    final public function get($key)
    {
        if (property_exists($this, $key)) {
            return $this->{$key};
        }

        if (($result = $this->getAssociated($key)) !== -1) {
            return $result;
        }

        throw new Exception('Access to unknown property/relation: ' . $key);
    }

    /**
     * @param string $key
     *
     * @return mixed
     * @throws \Exception
     */
    final public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * Method to get all properties (with optional associated models) as array.
     * If associated models are not fetched, they will be loaded.
     *
     * @param boolean $recursive Optionally load associated models.
     *
     * @return array
     */
    final public function toArray($recursive = true): array
    {
        $data = [];

        foreach ($this->__meta->getColumns() as $column) {
            $data[$column['name']] = $this->{$column['name']};
        }

        if ($recursive === true) {
            foreach ($this->__relations as $relation) {
                if (
                    !($relation->getHandler() instanceof HasManyRelationHandler)
                    && !($relation->getHandler() instanceof HasOneRelationHandler)
                    && !($relation->getHandler() instanceof ManyToManyRelationHandler)
                ) {
                    continue;
                }

                $result = $relation->getResult();

                if ($relation->hasResult() === false) {
                    $result = $relation->getHandler()->fetch();
                    $relation->setResult($result);
                }

                if ($result instanceof self) {
                    $result = $result->toArray(true);
                } elseif ($result instanceof ArrayCollection) {
                    $result = $result->toArray();
                }

                $data[$relation->getName()] = $result ?: null;
            }
        }

        return $data;
    }

    /**
     * Method to load associated models (if not already fetched and saved inside model cache)
     * Can be accessed through Entity::__get(string $key)
     *
     * @param string $key
     *
     * @return mixed
     */
    final public function getAssociated($key)
    {
        foreach ($this->__relations as $relation) {
            if ($relation->getName() === $key) {
                if ($relation->hasResult() === false) {
                    $result = $relation->getHandler()->fetch();
                    $relation->setResult($result);
                }

                return $relation->getResult();
            }
        }

        return -1;
    }

    /**
     * Returns the entities relations. Used in Repository::save
     *
     * @return array|Relation[]
     */
    final public function getRelations(): array
    {
        return $this->__relations;
    }

    /**
     * Creates an 1-n association
     *
     * @param string  $referenceClass
     * @param string  $referenceKey
     * @param string  $localKey
     * @param integer $limit
     *
     * @return Relation
     */
    final protected function hasMany($referenceClass, $referenceKey = null, $localKey = 'id', $limit = null): Relation
    {
        if ($referenceKey === null) {
            $referenceKey = $this->getReferenceKey($this);
        }

        $relation = new Relation($this->__app);
        $handler  = new HasManyRelationHandler($this);

        $relation->setName($referenceClass);
        $relation->setHandler($handler);

        $handler->configure((string)$referenceClass, (string)$referenceKey, (string)$localKey, (integer)$limit);

        $this->__relations[] = $relation;

        return $relation;
    }

    /**
     * Creates an 1-1 association
     *
     * @param string $referenceClass
     * @param string $referenceKey
     * @param string $localKey
     *
     * @return Relation
     */
    final protected function hasOne($referenceClass, $referenceKey = null, $localKey = 'id'): Relation
    {
        if ($referenceKey === null) {
            $referenceKey = $this->getReferenceKey($this);
        }

        $relation = new Relation($this->__app);
        $handler  = new HasOneRelationHandler($this);

        $relation->setName($referenceClass);
        $relation->setHandler($handler);

        $handler->configure((string)$referenceClass, (string)$referenceKey, (string)$localKey);

        $this->__relations[] = $relation;

        return $relation;
    }

    /**
     * Creates a n-n association
     *
     * @param string $referenceClass
     * @param string $referenceKey
     * @param string $referenceTargetKey
     * @param string $targetClass
     * @param string $targetKey
     *
     * @return Relation
     */
    final protected function manyToMany(
        $referenceClass,
        $referenceKey,
        $referenceTargetKey,
        $targetClass,
        $targetKey
    ): Relation {
        $relation = new Relation($this->__app);
        $handler  = new ManyToManyRelationHandler($this);

        $relation->setName($referenceClass);
        $relation->setHandler($handler);

        $handler->configure(
            (string)$referenceClass,
            (string)$referenceKey,
            (string)$referenceTargetKey,
            (string)$targetClass,
            (string)$targetKey
        );

        $this->__relations[] = $relation;

        return $relation;
    }

    /**
     * @param string|Entity $referenceClass
     *
     * @return string
     */
    private function getReferenceKey(string $referenceClass): string
    {
        return Utils::camelize($referenceClass::SOURCE) . 'ID';
    }

    /**
     * Creates an n-1 association.
     *
     * @param string $referenceClass
     * @param string $localKey
     * @param string $referenceKey
     *
     * @return Relation
     */
    final protected function belongsTo($referenceClass, $localKey = null, $referenceKey = 'id'): Relation
    {
        if ($localKey === null) {
            $localKey = $this->getReferenceKey($referenceClass);
        }

        $relation = new Relation($this->__app);
        $handler  = new BelongsToRelationHandler($this);

        $relation->setName($referenceClass);
        $relation->setHandler($handler);

        $handler->configure((string)$referenceClass, (string)$localKey, (string)$referenceKey);

        $this->__relations[] = $relation;

        return $relation;
    }

    // Array access implementation
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    public function offsetUnset($offset)
    {
        throw new Exception('offsetUnset is not implemented yet.');
    }

    public function offsetExists($offset)
    {
        if (property_exists($this, $offset)) {
            return true;
        }

        foreach ($this->__relations as $relation) {
            if ($relation->getName() === $offset) {
                return true;
            }
        }

        return false;
    }
}