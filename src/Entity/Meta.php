<?php

namespace Favez\ORM\Entity;

use Favez\ORM\Utils;
use PDOStatement;

class Meta
{
    /**
     * @var array
     */
    protected $columns = [];

    public static function create(Repository $repository): ?Meta
    {
        $sql  = Utils::sql('SELECT * FROM %s LIMIT 0', $repository->getSource());
        $stmt = $repository->getDB()->getPDO()->prepare($sql);

        if ($stmt->execute()) {
            return new self($stmt);
        }

        return null;
    }

    public function __construct(PDOStatement $stmt)
    {
        $this->columns = [];

        for ($i = 0; $i < $stmt->columnCount(); $i++) {
            $this->columns[] = $stmt->getColumnMeta($i);
        }
    }

    public function getColumns(): array
    {
        return $this->columns;
    }
}