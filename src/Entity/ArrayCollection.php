<?php

namespace Favez\ORM\Entity;

use ArrayAccess;
use Countable;
use Iterator;

class ArrayCollection implements Iterator, ArrayAccess, Countable
{
    protected $pos;

    protected $items;

    public function __construct(array $items = [])
    {
        $this->pos   = 0;
        $this->items = $items;
    }

    public function count(): int
    {
        return count($this->items);
    }

    public function toArray(): array
    {
        $result = [];

        foreach ($this->items as $item) {
            $result[] = $item->toArray();
        }

        return $result;
    }

    public function getIterator(): array
    {
        return $this->items;
    }

    public function add($item): ArrayCollection
    {
        $this->items[] = $item;

        return $this;
    }

    public function get(int $index)
    {
        if ($this->has($index)) {
            return $this->items[$index];
        }

        return null;
    }

    public function has(int $index): bool
    {
        return array_key_exists($index, $this->items);
    }

    public function clear(): ArrayCollection
    {
        $this->items = [];

        return $this;
    }

    public function remove($item): ArrayCollection
    {
        if ($index = $this->getIndex($item)) {
            $this->removeAt($index);
        } else {
            throw new \Exception('Item not found.');
        }

        return $this;
    }

    public function removeAt($index): ArrayCollection
    {
        unset ($this->items[$index]);

        return $this;
    }

    public function getIndex($item): ?int
    {
        foreach ($this->items as $i => $_item) {
            if ($_item === $item) {
                return $i;
            }
        }

        return null;
    }

    public function find(callable $filter)
    {
        foreach ($this->items as $i => $item) {
            if (call_user_func($filter, $item, $i) === true) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->get($this->pos);
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        ++$this->pos;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return $this->pos;
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return $this->has($this->pos);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->pos = 0;
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value)
    {
        $this->items[$offset] = $value;
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset)
    {
        unset ($this->items[$offset]);
    }
}