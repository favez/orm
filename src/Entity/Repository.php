<?php

namespace Favez\ORM\Entity;

use Exception;
use Favez\ORM\App;
use Favez\ORM\Entity\ArrayCollection;

class Repository
{
    /**
     * @var App
     */
    protected $app;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var Meta
     */
    protected $meta;

    public function __construct(App $app, $entity)
    {
        $this->app    = $app;
        $this->entity = $entity;
        $this->source = ($this->entity)::SOURCE;
        $this->meta   = Meta::create($this);
    }

    /**
     * Finds the first entity identified by its id.
     *
     * @param integer $primaryKey
     *
     * @return Entity|null
     * @throws Exception
     */
    public function find($primaryKey): ?Entity
    {
        return $this->findOneBy([ 'id' => (int)$primaryKey ]);
    }

    /**
     * Finds the first entity identified by the given criteria.
     *
     * @param array|string $criteria
     *
     * @return Entity|null
     *
     * @throws \Exception
     */
    public function findOneBy($criteria): ?Entity
    {
        $result = $this->app->getDriver()->from($this->source)->where($criteria)->fetch();

        if (empty($result)) {
            return null;
        }

        /** @var Entity $model */
        $model = $this->create();
        $model->set($result);

        $this->triggerHook($model, 'onFetch');

        return $model;
    }

    /**
     * Find all entities.
     *
     * @param string $orderBy
     *
     * @return array|\Favez\ORM\Entity\ArrayCollection
     *
     * @throws Exception
     */
    public function findAll($orderBy = null)
    {
        return $this->findBy([], $orderBy);
    }

    /**
     * Finds all entities identified by the given criteria.
     *
     * @param array|string $criteria
     * @param string       $orderBy
     *
     * @return ArrayCollection
     * @throws \Exception
     */
    public function findBy($criteria, $orderBy = null): ArrayCollection
    {
        $query = $this->app->getDriver()->from($this->source);
        $query->where($criteria);

        if ($orderBy !== null) {
            $query->orderBy($orderBy);
        }

        $result     = $query->fetchAll();
        $collection = new ArrayCollection();

        foreach ($result as $data) {
            $model = $this->create();
            $model->set($data);

            $this->triggerHook($model, 'onFetch');

            $collection->add($model);
        }

        return $collection;
    }

    /**
     * Creates an empty instance of the entity.
     *
     * @return Entity
     */
    public function create(): Entity
    {
        return new $this->entity($this->app, $this->meta);
    }

    /**
     * Inserts or updates the entity to database.
     *
     * @param Entity $entity
     *
     * @throws \Exception
     */
    public function save(Entity $entity): void
    {
        if ((int)$entity->id > 0) {
            $this->triggerHook($entity, 'onSave');

            $values = $entity->toArray(false);

            unset ($values['id']);

            $this->app->getDriver()->update($this->source, $values, $entity->id)->execute();

            // Insert/update related entities
            foreach ($entity->getRelations() as $relation) {
                if ($relation->getHandler()->shouldUpdate()) {
                    $relation->getHandler()->save();
                }
            }
        } else {
            $this->triggerHook($entity, 'onSave');

            $values   = $entity->toArray(false);
            $insertID = $this->app->getDriver()->insertInto($this->source, $values)->execute();

            if ($insertID === false) {
                throw new Exception('#500 Insert failed');
            }

            $entity->set('id', (int)$insertID);

            // Insert/update related entities
            foreach ($entity->getRelations() as $relation) {
                if ($relation->getHandler()->shouldUpdate()) {
                    $relation->getHandler()->save();
                }
            }
        }
    }

    /**
     * Removes the entity from database permanently.
     *
     * @param Entity|ArrayCollection $entity
     */
    public function remove($entity): void
    {
        if ($entity instanceof ArrayCollection) {
            foreach ($entity as $item) {
                $this->remove($item);
            }

            return;
        }

        $this->triggerHook($entity, 'onRemove');

        $this->app->getDriver()->delete($this->source, $entity->id)->execute();

        // Remove related entities
        foreach ($entity->getRelations() as $relation) {
            if ($relation->getHandler()->shouldRemove()) {
                $relation->getHandler()->remove();
            }
        }

        $entity->id = null;
    }

    /**
     * Refreshes the given entity with fresh data from database.
     *
     * @param Entity $entity
     *
     * @throws \Exception
     */
    public function refresh($entity): void
    {
        if (property_exists($entity, 'id') === false) {
            throw new Exception('#500 Missing required property `id` in entity');
        }

        $row = $this->app->getDriver()->from($this->source, $entity->id)->fetch();

        $entity->set($row);
        $entity->id = (int)$entity->id;

        $this->triggerHook($entity, 'onFetch');

        // Refresh related entities
        foreach ($entity->getRelations() as $relation) {
            if ($relation->getHandler()->shouldRefresh()) {
                $relation->getHandler()->refresh();
            }
        }
    }

    /**
     * Returns the table for the entity.
     *
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    public function getDB(): App
    {
        return $this->app;
    }

    /**
     * This method calls a dynamic function of a random entity.
     *
     * It is used to implement events/hooks for models like onSave, onFetch or onDelete
     *
     * @param Entity $entity
     * @param string $event
     * @param array  $arguments
     */
    protected function triggerHook(Entity $entity, $event, $arguments = []): void
    {
        if (method_exists($entity, $event)) {
            call_user_func_array([ $entity, $event ], $arguments);
        }
    }
}