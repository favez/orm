<?php

namespace Favez\ORM\Entity;

use Favez\ORM\App;
use Favez\ORM\Entity\Relation\Handler;

class Relation
{
    
    /**
     * @var App
     */
    protected $app;
    
    /**
     * A user defined name for the relation.
     *
     * @var string
     */
    protected $name;
    
    /**
     * The relation handler
     *
     * @var Handler
     */
    protected $handler;
    
    /**
     * @var ArrayCollection|Entity|null
     */
    protected $result;
    
    public function __construct($app)
    {
        $this->app = $app;
    }
    
    public function setName ($name): void
    {
        $this->name = $name;
    }
    
    public function getName (): string
    {
        return $this->name;
    }
    
    public function setHandler ($handler): void
    {
        $this->handler = $handler;
        $this->handler->setRelation($this);
        $this->handler->setApp($this->app);
    }
    
    /**
     * @param mixed $result
     */
    public function setResult ($result): void
    {
        $this->result = $this->handler->processResult($result);
    }
    
    public function hasResult (): bool
    {
        return !empty($this->result);
    }
    
    public function getResult ()
    {
        return $this->result;
    }
    
    public function getHandler (): Handler
    {
        return $this->handler;
    }

}