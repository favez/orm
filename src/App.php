<?php

namespace Favez\ORM;

use Favez\ORM\Entity\Entity;
use Favez\ORM\Entity\Repository;
use FluentPDO;
use PDO;

class App
{
    
    /**
     * @var PDO
     */
    protected $pdo;
    
    /**
     * @var \FluentPDO
     */
    protected $driver;
    
    /**
     * @var Repository[]
     */
    protected $repositories = [];
    
    public function __construct (PDO $pdo = null)
    {
        if ($pdo instanceof PDO)
        {
            $this->setPDO($pdo);
        }
    }
    
    public function getPDO (): PDO
    {
        return $this->pdo;
    }
    
    public function setPDO (PDO $pdo): bool
    {
        if (!($this->pdo instanceof PDO))
        {
            $this->pdo    = $pdo;
            $this->driver = new FluentPDO($this->pdo);
            
            return true;
        }
        
        return false;
    }
    
    /**
     * Returns the internal driver which MUST be an instance of \FluentPDO
     *
     * @return \FluentPDO
     */
    public function getDriver (): FluentPDO
    {
        return $this->driver;
    }
    
    /**
     * Creates a singleton instance of a repository and returns it.
     *
     * @param string $entity
     *
     * @return Repository
     */
    public function getRepository ($entity): Repository
    {
        if (isset($this->repositories[$entity]) === false)
        {
            $this->repositories[$entity] = new Repository($this, $entity);
        }
        
        return $this->repositories[$entity];
    }

    public function isValid ($input): bool
    {
        return $input instanceof Entity;
    }
    
}